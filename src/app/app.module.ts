import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule} from "@angular/forms";
import {StoreModule} from "@ngrx/store";
import { organizationsReducer } from "./redux/organization.reducer";
import { OrganizationComponent } from './organization/organization.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule, MatInputModule} from "@angular/material";
import { OrganizationAddComponent } from './organization/organization-add/organization-add.component';
import {MatButtonModule} from "@angular/material/button";
import { OrganizationEditComponent } from './organization/organization-edit/organization-edit.component';
import { FilialEditComponent } from './organization/filial-edit/filial-edit.component';
import { FilialAddComponent } from './organization/filial-add/filial-add.component';
import {MatSelectModule} from "@angular/material/select";
import {MatExpansionModule} from '@angular/material/expansion';
import {AuthService} from "./auth/auth.service";
import {AuthGuard, AuthGuardAdmin} from "./auth/auth-guard.service";
import { LoginComponent } from './auth/login/login.component';
import { TreeComponent } from './organization/tree/tree.component';
import {TreeModule} from "angular-tree-component";
import { HeaderComponent } from './header/header.component';
import {MatTabsModule} from "@angular/material/tabs";
import {MatToolbarModule} from "@angular/material/toolbar";
import { LogoutComponent } from './auth/logout/logout.component';

@NgModule({
  declarations: [
    AppComponent,
    OrganizationComponent,
    OrganizationAddComponent,
    OrganizationEditComponent,
    FilialEditComponent,
    FilialAddComponent,
    LoginComponent,
    TreeComponent,
    HeaderComponent,
    LogoutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    StoreModule.forRoot({organizationPage: organizationsReducer}),
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatExpansionModule,
    MatTabsModule,
    MatToolbarModule,
    TreeModule.forRoot()
  ],
  providers: [AuthService, AuthGuard, AuthGuardAdmin],
  bootstrap: [AppComponent]
})
export class AppModule { }
