import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {OrganizationComponent} from "./organization/organization.component";
import {OrganizationAddComponent} from "./organization/organization-add/organization-add.component";
import {OrganizationEditComponent} from "./organization/organization-edit/organization-edit.component";
import {FilialAddComponent} from "./organization/filial-add/filial-add.component";
import {FilialEditComponent} from "./organization/filial-edit/filial-edit.component";
import {AuthGuard, AuthGuardAdmin} from "./auth/auth-guard.service";
import {LoginComponent} from "./auth/login/login.component";
import {TreeComponent} from "./organization/tree/tree.component";
import {LogoutComponent} from "./auth/logout/logout.component";

const routes: Routes = [
  { path: 'login', component: LoginComponent},
  { path: 'logout', component: LogoutComponent},
  { path: 'organization', component: OrganizationComponent, canActivate: [AuthGuard] },
  { path: 'organization-tree', component: TreeComponent, canActivate: [AuthGuard] },
  { path: 'organization/add', component: OrganizationAddComponent, canActivate: [AuthGuardAdmin] },
  { path: 'organization/addfilial', component: FilialAddComponent, canActivate: [AuthGuardAdmin] },
  { path: 'organization/edit/:id', component: OrganizationEditComponent, canActivate: [AuthGuardAdmin] },
  { path: 'organization/editfilial/:id', component: FilialEditComponent, canActivate: [AuthGuardAdmin] },
  { path: '**', redirectTo: 'login'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
