import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {NgForm} from "@angular/forms";
import {AuthService} from "../auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  errorMsg: string;

  constructor(private auth: AuthService,
              private router: Router) { }

  navigateToListOrganizations() {
    this.router.navigate(['/organization'])
  }


  submitForm(form: NgForm) {
    if (this.auth.login(form.value.login, form.value.password)) {
      this.navigateToListOrganizations();
    } else {
      this.errorMsg = 'Неправильный логин или пароль'
    }

  }

  ngOnInit() {
    this.auth.logout();
  }

}
