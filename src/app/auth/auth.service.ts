export class AuthService {
    isLoggedIn = false;
    isAdmin = false;

    public links = [
        {path: '/login', name: 'Вход', hidden: false},
        {path: '/organization', name: 'Список организаций', hidden: true},
        {path: '/organization-tree', name: 'Дерево организаций', hidden: true},
        {path: '/organization/add', name: 'Создать организацию', hidden: true , isAdmin: true},
        {path: '/organization/addfilial', name: 'Создать филиал', hidden: true, isAdmin: true},
        {path: '/logout', name: 'Выйти', hidden: true},
    ];

    isAuth() {
        return new Promise((resolve => {
            setTimeout(() => {
                resolve(this.isLoggedIn);
            }, 20);
        }))
    }

    isAuthAdmin() {
        return new Promise((resolve => {
            setTimeout(() => {
                resolve(this.isAdmin);
            }, 20);
        }))
    }

    login(login: string, password: string) {
        if (login === 'admin' && password === 'admin') {
            this.isLoggedIn = true;
            this.isAdmin = true;
            this.links.map(item => item.hidden = !item.hidden);
            return true;
        } else if (login === 'test' && password === 'test') {
            this.isLoggedIn = true;
            this.isAdmin = false;
            this.links.map(item => {
                if (!item.isAdmin) {
                    item.hidden = !item.hidden
                }
            });
            return true;
        } else {
            return false;
        }
    }

    logout() {
        this.isLoggedIn = false;
        this.isAdmin = false;
        this.links.map(item => item.hidden = true);
        this.links[0].hidden = false;
    }
}
