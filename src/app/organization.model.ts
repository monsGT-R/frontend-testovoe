export class Organization {
  constructor(
    public name: string,
    public fullName: string,
    public inn: string,
    public kpp: string,
    public fio: string,
    public address: string,
    public phone: string,
    public id?: number,
    public parentId: number = null,
    public isFilial: boolean = false,
  ) {}
}

export interface Organizations {
  organizations: Organization[]
}
