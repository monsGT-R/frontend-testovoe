import { Component, OnInit } from '@angular/core';
import {Organization} from "../../organization.model";
import {Store} from "@ngrx/store";
import {AppState} from "../../redux/app.state";

@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.css']
})
export class TreeComponent implements OnInit {
  organizations: Organization[];
  filials: Organization[];
  nodes = [];
  options = {};

  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    this.store.select('organizationPage')
        .subscribe(({organizations}) => {
          this.organizations = organizations.filter(o => !o.isFilial);
          this.filials = organizations.filter(o => o.isFilial);
          this.createTree();
        });
  }

  createTree() {
    this.organizations.forEach(item => {
      const filials = this.filials.filter(f => f.parentId === item.id);
      const childrens = [];
      filials.forEach(f => childrens.push({name: f.name}));
      this.nodes.push({name: item.name, children: childrens})
    })
  }

}
