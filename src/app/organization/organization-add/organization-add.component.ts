import { Component,  } from '@angular/core';
import {NgForm} from "@angular/forms";
import {Store} from "@ngrx/store";
import {AppState} from "../../redux/app.state";
import {AddOrganization} from "../../redux/organization.action";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-organization-add',
  templateUrl: './organization-add.component.html',
  styleUrls: ['./organization-add.component.css']
})
export class OrganizationAddComponent {

  constructor(private store: Store<AppState>,
              private router: Router,
              private route: ActivatedRoute) { }

  navigateToListOrganizations() {
    this.router.navigate(['../'], {relativeTo: this.route})
  }


  submitForm(form: NgForm) {
    this.store.dispatch(new AddOrganization(form.value));
    this.navigateToListOrganizations();
  }

}
