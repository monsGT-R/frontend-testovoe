import {Component, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {AppState} from "../../redux/app.state";
import {ActivatedRoute, Router} from "@angular/router";
import {NgForm} from "@angular/forms";
import {AddFilial} from "../../redux/organization.action";
import {Organization} from "../../organization.model";

@Component({
  selector: 'app-filial-add',
  templateUrl: './filial-add.component.html',
  styleUrls: ['./filial-add.component.css']
})
export class FilialAddComponent implements OnInit{
  organizations: Organization[];

  constructor(private store: Store<AppState>,
              private router: Router,
              private route: ActivatedRoute) { }

  navigateToListOrganizations() {
    this.router.navigate(['../'], {relativeTo: this.route})
  }


  submitForm(form: NgForm) {
    this.store.dispatch(new AddFilial(form.value));
    this.navigateToListOrganizations();
  }

  ngOnInit() {
    this.store.select('organizationPage')
        .subscribe(({organizations}) => this.organizations = organizations.filter(o => !o.isFilial));
  }
}
