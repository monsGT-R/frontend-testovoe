import {Component, Input, OnInit} from '@angular/core';
import {Organization, Organizations} from "../organization.model";
import {Store} from "@ngrx/store";
import {AppState} from "../redux/app.state";
import {DelOrganization, UpdateOrganization} from "../redux/organization.action";
import {Observable} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthService} from "../auth/auth.service";

@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.css']
})
export class OrganizationComponent implements OnInit{
  organizationState: Observable<Organizations>;
  isAdmin;

  constructor(private store: Store<AppState>, private auth: AuthService) { }

  onDelete(organization) {
    this.store.dispatch(new DelOrganization(organization));
  }

  ngOnInit() {
    this.auth.isAuthAdmin().then((isAdmin: boolean) => {
      this.isAdmin = isAdmin;
    });
    this.organizationState = this.store.select('organizationPage')
  }

}
