import {Component, OnInit, ViewChild} from '@angular/core';
import {Store} from "@ngrx/store";
import {AppState} from "../../redux/app.state";
import {ActivatedRoute, Router} from "@angular/router";
import {NgForm} from "@angular/forms";
import {UpdateOrganization} from "../../redux/organization.action";
import {Organization} from "../../organization.model";

@Component({
  selector: 'app-organization-edit',
  templateUrl: './organization-edit.component.html',
  styleUrls: ['./organization-edit.component.css']
})
export class OrganizationEditComponent implements OnInit{

  @ViewChild('form') form: NgForm;
  id: number;
  organization: Organization;

  constructor(private store: Store<AppState>,
              private router: Router,
              private route: ActivatedRoute) { }

  navigateToListOrganizations() {
    this.router.navigate(['../../'], {relativeTo: this.route})
  }

  submitForm() {
    this.organization = { ...this.organization, ...this.form.value };
    this.store.dispatch(new UpdateOrganization(this.organization));
    this.navigateToListOrganizations();
  }

  ngOnInit() {
    this.id = +this.route.snapshot.params['id'];

    this.store.select('organizationPage')
        .subscribe(({organizations}) => {
          this.organization = organizations.find(o => o.id === this.id && !o.isFilial);

          if (this.organization === undefined) {
            this.navigateToListOrganizations();
            return;
          }

          setTimeout(() => this.form.form.patchValue(this.organization));
        });
  }

}
