import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from "@angular/forms";
import {Organization} from "../../organization.model";
import {Store} from "@ngrx/store";
import {AppState} from "../../redux/app.state";
import {ActivatedRoute, Router} from "@angular/router";
import {UpdateFilial} from "../../redux/organization.action";

@Component({
  selector: 'app-filial-edit',
  templateUrl: './filial-edit.component.html',
  styleUrls: ['./filial-edit.component.css']
})
export class FilialEditComponent implements OnInit {

  @ViewChild('form') form: NgForm;
  id: number;
  organization: Organization;
  organizations: Organization[];


  constructor(private store: Store<AppState>,
              private router: Router,
              private route: ActivatedRoute) { }

  navigateToListOrganizations() {
    this.router.navigate(['../../'], {relativeTo: this.route})
  }

  submitForm() {
    this.organization = { ...this.organization, ...this.form.value };
    this.store.dispatch(new UpdateFilial(this.organization));
    this.navigateToListOrganizations();
  }

  ngOnInit() {
    this.id = +this.route.snapshot.params['id'];

    this.store.select('organizationPage')
        .subscribe(({organizations}) => {
          this.organizations = organizations.filter(o => !o.isFilial);

          this.organization = organizations.find(o => o.id === this.id && o.isFilial);
          if (this.organization === undefined) {
            this.navigateToListOrganizations();
            return;
          }

          setTimeout(() => this.form.form.patchValue(this.organization));
        });
  }

}
