import {Action} from '@ngrx/store'
import {Organization} from '../organization.model'

export namespace ORGANIZATION_ACTION {
  export const ADD = 'ADD';
  export const DEL = 'DEL';
  export const UPDATE = 'UPDATE';
  export const ADD_FILIAL = 'ADD_FILIAL';
  export const UPDATE_FILIAL = 'UPDATE_FILIAL';
}

export class AddOrganization implements Action {
  readonly type = ORGANIZATION_ACTION.ADD;

  constructor(public payload: Organization) {}
}

export class DelOrganization implements Action {
  readonly type = ORGANIZATION_ACTION.DEL;

  constructor(public payload: Organization) {}
}

export class UpdateOrganization implements Action {
  readonly type = ORGANIZATION_ACTION.UPDATE;

  constructor(public payload: Organization) {}
}

export class AddFilial implements Action {
  readonly type = ORGANIZATION_ACTION.ADD_FILIAL;

  constructor(public payload: Organization) {}
}

export class UpdateFilial implements Action {
  readonly type = ORGANIZATION_ACTION.UPDATE_FILIAL;

  constructor(public payload: Organization) {}
}
export type OrganizationsAction = AddOrganization | DelOrganization | UpdateOrganization | AddFilial | UpdateFilial;
