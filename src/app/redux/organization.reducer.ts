import {Organization} from '../organization.model'
import {ORGANIZATION_ACTION, OrganizationsAction} from './organization.action'

const initialState = {
  organizations: [
    new Organization(
        'ООО "СТМ"',
        'ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ "СТРОЙТОРГМОНТАЖ"',
        '3525149800', '1053500034939', 'Голубев Николай Леонидович',
        '160000, ВОЛОГОДСКАЯ ОБЛ, ВОЛОГДА Г, МОЖАЙСКОГО УЛ, 72, 70',
        '722025', 1),
    new Organization(
        '"ФОТОЮНОСТЬ"',
        'АВТОНОМНАЯ НЕКОММЕРЧЕСКАЯ ОРГАНИЗАЦИЯ ДЕТСКОЙ ФОТОГРАФИИ "ФОТОЮНОСТЬ"',
        '3525406782', '1173525025629', 'Зайцев Артем Николаевич',
        '160024, ВОЛОГОДСКАЯ ОБЛ, ВОЛОГДА Г, СЕВЕРНАЯ УЛ, ДОМ 36, ПОМЕЩЕНИЕ 5',
        '921-2383700', 2),
    new Organization(
        '"ГСК "ЮРМАЛА"',
        'НЕКОММЕРЧЕСКАЯ ОРГАНИЗАЦИЯ ПОТРЕБИТЕЛЬСКОЕ ГАРАЖНО-СТРОИТЕЛЬНОЕ ОБЩЕСТВО "ГСК "ЮРМАЛА"',
        '7724048627', '1053500034939', 'Аросев Антон Владиславович',
        '115580, г Москва, Южный административный округ, район Зябликово, ул Мусы Джалиля, д 29, корп 1',
        '+7(903)684-36-54', 3),
    new Organization(
        'ООО "МУСТАНГ"',
        'ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ "МУСТАНГ"',
        '7724048627', '1053500034939', 'Сайфутдинова Елена Витальевна',
        '115580, Г МОСКВА, УЛ ШИПИЛОВСКАЯ, Д 60, КОРП 1',
        '+7(923)684-56-54', 4),
    new Organization(
        'ООО "МУСТАНГ" (филиал "Иванов Василий Николаевич")',
        '', '', '',
        'Иванов Василий Николаевич',
        '115580, Г МОСКВА, УЛ ШИПИЛОВСКАЯ, Д 60, КОРП 1',
        '+7(923)684-56-54', 16, 4, true),
    new Organization(
        'ООО "НАКО-ТВИК"',
        'ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ "МУСТАНГ"',
        '7724048627', '1053500034939', 'Сайфутдинова Елена Витальевна',
        '115580, Г МОСКВА, УЛ ШИПИЛОВСКАЯ, Д 60, КОРП 1',
        '+7(923)684-56-54', 5),
    new Organization(
        'ООО "Группа-МКЛ" (филиал "Петров Виталий Леонидович")',
        '', '', '',
        'Петров Виталий Леонидович',
        '115580, Г МОСКВА, УЛ ШИПИЛОВСКАЯ, Д 60, КОРП 1',
        '+7(923)684-56-54', 13, 9, true),
    new Organization(
        'ООО "Стройинжсервис"',
        'ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ "МУСТАНГ"',
        '7724048627', '1053500034939', 'Сайфутдинова Елена Витальевна',
        '115580, Г МОСКВА, УЛ ШИПИЛОВСКАЯ, Д 60, КОРП 1',
        '+7(923)684-56-54', 6),
    new Organization(
        'ООО "МУСТАНГ" (филиал "Петров Виталий Леонидович")',
        '', '', '',
        'Петров Виталий Леонидович',
        '115580, Г МОСКВА, УЛ ШИПИЛОВСКАЯ, Д 60, КОРП 1',
        '+7(923)684-56-54', 14, 4, true),
    new Organization(
        'ООО "ТД "МИССАРОНИ"',
        'ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ "МУСТАНГ"',
        '7724048627', '1053500034939', 'Сайфутдинова Елена Витальевна',
        '115580, Г МОСКВА, УЛ ШИПИЛОВСКАЯ, Д 60, КОРП 1',
        '+7(923)684-56-54', 7),
    new Organization(
        'ООО "ЮЛАЙСИМПОРТ"',
        'ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ "МУСТАНГ"',
        '7724048627', '1053500034939', 'Сайфутдинова Елена Витальевна',
        '115580, Г МОСКВА, УЛ ШИПИЛОВСКАЯ, Д 60, КОРП 1',
        '+7(923)684-56-54', 8),
    new Organization(
        'ООО "Группа-МКЛ" (филиал "Иванов Василий Николаевич")',
        '', '', '',
        'Иванов Василий Николаевич',
        '115580, Г МОСКВА, УЛ ШИПИЛОВСКАЯ, Д 60, КОРП 1',
        '+7(923)684-56-54', 12, 9, true),
    new Organization(
        'ООО "Группа-МКЛ"',
        'ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ "МУСТАНГ"',
        '7724048627', '1053500034939', 'Сайфутдинова Елена Витальевна',
        '115580, Г МОСКВА, УЛ ШИПИЛОВСКАЯ, Д 60, КОРП 1',
        '+7(923)684-56-54', 9),
    new Organization(
        'ООО "МУСТАНГ" (филиал "Сайфутдинова Елена Витальевна")',
        '', '', '',
        'Сайфутдинова Елена Витальевна',
        '115580, Г МОСКВА, УЛ ШИПИЛОВСКАЯ, Д 60, КОРП 1',
        '+7(923)684-56-54', 15, 4, true),
    new Organization(
        'ООО "ВестПром"',
        'ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ "МУСТАНГ"',
        '7724048627', '1053500034939', 'Сайфутдинова Елена Витальевна',
        '115580, Г МОСКВА, УЛ ШИПИЛОВСКАЯ, Д 60, КОРП 1',
        '+7(923)684-56-54', 10),
    new Organization(
        'ООО "Группа-МКЛ" (филиал "Голубев Николай Леонидович")',
        '', '', '',
        'Голубев Николай Леонидович',
        '115580, Г МОСКВА, УЛ ШИПИЛОВСКАЯ, Д 60, КОРП 1',
        '+7(923)684-56-54', 11, 9, true),
  ]
};

export function organizationsReducer(state = initialState, action: OrganizationsAction) {
  switch (action.type) {
    case ORGANIZATION_ACTION.ADD:
      action.payload.id = state.organizations.length + 1;
      return {
        ...state,
        organizations: [...state.organizations, action.payload]
      };
    case ORGANIZATION_ACTION.DEL:
      return {
        ...state,
        organizations: [...state.organizations.filter(o => o.id !== action.payload.id)]
      };
    case ORGANIZATION_ACTION.UPDATE:
      const idx = state.organizations.findIndex(o => o.id === action.payload.id);
      state.organizations[idx]= action.payload;
      return {
        ...state,
        organizations: [...state.organizations]
      };
    case ORGANIZATION_ACTION.ADD_FILIAL:
      const organization = state.organizations.find(o => o.id == action.payload.parentId);
      action.payload.id = state.organizations.length + 1;
      action.payload.name =  `${organization.name} (филиал "${action.payload.fio}")`;
      action.payload.isFilial = true;
      return {
        ...state,
        organizations: [...state.organizations, action.payload]
      };
    case ORGANIZATION_ACTION.UPDATE_FILIAL:
      const idxF = state.organizations.findIndex(o => o.id === action.payload.id);
      const organizationu = state.organizations.find(o => o.id == action.payload.parentId);
      action.payload.name = `${organizationu.name} (филиал "${action.payload.fio}")`;
      state.organizations[idxF]= action.payload;
      return {
        ...state,
        organizations: [...state.organizations]
      };

    default:
      return state
  }
}
