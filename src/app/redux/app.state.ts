import {Organization} from '../organization.model'

export interface AppState {
  organizationPage: {
    organizations: Organization[]
  }
}
